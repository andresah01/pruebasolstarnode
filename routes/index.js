var express = require('express')
var router = express.Router()
const axios = require('axios')


router.get('/', async (req, res)=>{    
  res.render('cantantes/index', {    
    title: 'Cantantes'
  });
});

router.get('/nuevo', (req, res)=>{     
  res.render('cantantes/nuevo', {
    title: 'Nuevo'
  })
});

module.exports = router;
