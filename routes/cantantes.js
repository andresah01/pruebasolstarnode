var express = require('express')
var router = express.Router()
const cantanteController = require('../controllers/cantanteController')
const multer = require('multer')
const validateSinger = require('../validators/cantantesValidator')
const fecha = Date.now()


const ruta = multer.diskStorage({
    destination: function(request, file, callback){
        callback(null, './public/images/')
    },
    filename: function(request, file, callback){        
        callback(null, `${fecha}_${file.originalname}`)
    }
})
const cargar = multer({
    storage: ruta
})   

router.get('/all', cantanteController.index);
router.post('/store', cargar.single('foto'), validateSinger,cantanteController.store);
router.post('/delete/:id', cantanteController.delete)
router.post('/info/:id', cantanteController.info)
router.post('/update', cargar.single('foto'), cantanteController.update)

module.exports = router;
