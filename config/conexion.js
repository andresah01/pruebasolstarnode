const mysql = require('mysql')

const conexion = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'prueba_solstar'
})

conexion.connect((error)=>{
    !error ?  console.log('Connected') :  console.log(error)   
})

module.exports = conexion