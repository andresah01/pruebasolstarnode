const {check} = require ('express-validator')
const validateResult = require('../helper/validateHelper')

const validateSinger = [
    check('nombre').exists().not().isEmpty().withMessage('El campo Nombre debe ser llenado'),
    check('biografia').exists().not().isEmpty().withMessage('El campo Biografia debe ser llenado'),
    check('fechaNacimiento').exists().not().isEmpty().withMessage('El campo Fecha de Nacimiento debe ser llenado'),
    check('genero').exists().not().isEmpty().withMessage('El campo Genero debe ser llenado'),
    (req, res, next) => {
        validateResult(req, res, next)
    }

]

module.exports = validateSinger