const conexion = require('../config/conexion')
const cantante = require('../model/cantante')
const borrar = require('fs')

module.exports={
    index: (req, res) =>{
        cantante.getAll(conexion,(error, datos)=>{              
            res.send(datos);
        })        
    },
    store: (req, res) =>{ 
        cantante.store(conexion, req.body, req.file, (error, datos) => { 
            res.send('Exito')
        })        
    },
    info:(req, res) =>{
        cantante.info(conexion, req.params.id, (error, datos)=>{        
           res.send(datos)
        })        
    },
    delete: (req, res) =>{
        cantante.info(conexion, req.params.id, (error, datos)=>{   
            const foto = `public/images/${datos[0].foto}`
            if(borrar.existsSync(foto)){
                borrar.unlinkSync(foto)  
            }            
        })      
        cantante.delete(conexion, req.params.id, (error, datos)=>{
            res.send(datos)
        })
    },
    update: (req, res) =>{ 
        if (req.file){
            if(req.file.filename){
                cantante.info(conexion, req.body.id, (error, datos)=>{   
                    const foto = `public/images/${datos[0].foto}`
                    if(borrar.existsSync(foto)){
                        borrar.unlinkSync(foto)  
                    }            
                })  
                cantante.updateFile(conexion, req.body, req.file, (error, datos)=>{
                })
            }             
            
        }       
        cantante.update(conexion, req.body, (error, datos)=>{            
            res.send(error)
        })
    }

    
}