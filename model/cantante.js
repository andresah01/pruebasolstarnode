module.exports = {
    getAll: (conexion, funcion) => {
        conexion.query('select * from cantantes', funcion)
    },
    store: (conexion, data, file, funcion) => {
        conexion.query('insert into cantantes (nombre, fechaNacimiento, biografia, foto, genero) values (?,?,?,?,?)',[data.nombre, data.fechaNacimiento, data.biografia, file.filename, data.genero], funcion)
    },
    info: (conexion, id, funcion) => {
        conexion.query('select * from cantantes where id =?', [id], funcion)
    },
    delete: (conexion, id, funcion) => {
        conexion.query('delete from cantantes where id =?', [id], funcion)
    },
    update: (conexion, data, funcion) => {
        conexion.query('update cantantes set nombre =?, fechaNacimiento =?, biografia =?, genero =? where id =?', [data.nombre, data.fechaNacimiento, data.biografia, data.genero, data.id], funcion)
    },
    updateFile: (conexion, data, file, funcion) => {
        conexion.query('update cantantes set foto =? where id =?', [file.filename, data.id], funcion)
    }
}